# Transposition du rattrapages de l'année 2020

1. Forkez le projet
    * URL du projet sur GitLab : https://gitlab.com/edouard-paumier/rattrapages-2020
2. Clonez-le localement (avec GitKraken ou SourceTree par exemple)
3. Créez une Merge Request de votre master vers votre prod
4. Ouvrez le projet dans IntelliJ
5. Rafraîchissez la configuration du projet à partir du pom.xml
6. Suivez le [sujet d'examen](https://www.myefrei.fr/moodle/mod/quiz/view.php?id=34903)
7. Comitez vos changements
8. Soumettez une Merge Request de **votre** master vers **votre** prod
9. Vérifiez que votre Merge Request comprend bien vos changements (onglet changes)
10. Vérifier que `Edouard Paumier (@edouard-paumier)` a bien accès à votre projet.
11. Soumettez le Quizz
