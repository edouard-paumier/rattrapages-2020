package fr.efrei.rattrapages.rattrapage2020;

import java.util.ArrayList;
import java.util.List;

public class Amelioration1 {
    /*
        La méthode suivante a pour but d’identifier un groupe de note considérées comme représentatives
        de l’ensemble, et ce via un algorithme relativement basique.

        Elle reçoit une liste de notes, allant de 0 à 20, et les divise en groupe de tailles égale.

        Le second paramètre qualifie le nombre de groupes.
        S’il vaut 2, il y aura deux groupes, le premier regroupant les notes allant de 0 à 10,
        le second de 10 à 20.
        S’il vaut 8, il y aura 8 groupes, le premier allant de 0 à (20/8=2,5), le second de 2,5 à 5,
        le troisième de 5 à 7,5… et le 8ème regroupera les notes allant de 17,5 à 20.

        Une fois regroupée, la méthode trouve le groupe comportant le plus de notes, et retourne ces
        dernières.
        L’idée est que si le 5ème groupe a le plus de notes, alors on peut dire que de nombreux élèves
        ont des notes similaires.
     */
    public static List<Double> trouverNotesRepresentatives(List<Double> l1, int i) {
        List<List<Double>> l2 = new ArrayList<List<Double>>();

        Double j = 20.0 / i;
        // Preparation des listes
        for (int m = 0; m < i; m++) {
            l2.add(new ArrayList<Double>());
        }

        // Groupement des notes par listes
        for (Double n: l1) {
            for (int m = 0; m < i; m++) { Double k = (m + 1) * j;
                if (n <= k) {
                    l2.get(m).add(n);
                    break;
                }
            }
        }

        // Détermination de la liste la plus chargée
        int o = l2.get(0).size();
        int p = 0;
        for (int m = 0; m < i; m++) {
            if (l2.get(m).size() > o) {
                p = m;
                o = l2.get(m).size();
            }
        }

        // Retour de la liste la plus chargée
        return l2.get(p);
    }
}
